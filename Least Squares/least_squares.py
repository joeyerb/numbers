#!/usr/bin/env python3

import numpy as np
import pylab as pl
from scipy import linalg

def least_square(xs, ys, n):

    if len(xs) != len(ys):
        return
    m = len(xs)

    ms = []
    for j in range(2*n+1):
        s = 0
        for i in range(m):
            s += xs[i] ** j
        ms.append(s)

    bs = []
    for j in range(n+1):
        s = 0
        for i in range(m):
            s += xs[i] ** j * ys[i]
        bs.append(s)

    mtrx = []
    for j in range(n+1):
        row = []
        for i in range(n+1):
            row.append(ms[i+j])
        mtrx.append(row)

    sa = linalg.solve(np.array(mtrx), np.array(bs))
    print(sa)

    def f(x):
        s = 0
        for j in range(n+1):
            s += sa[j] * x**j
        return s

    return f


# P106.L5
x1 = [1.0, 2.0, 4.0, 6.0, 8.0, 10.0]
y1 = [1.8, 3.7, 8.2, 12.0, 15.8, 20.2]
f1 = least_square(x1, y1, 1)

pl.plot(x1, y1, label='f1 raw')
x1s = np.linspace(x1[0], x1[-1], 101)
pl.plot(x1s, list(f1(x) for x in x1s), label='f1 fitted')

# P184.10
x2 = [1, 3, 4, 5, 6, 7, 8, 9, 10]
y2 = [10, 5, 4, 2, 1, 1, 2, 3, 4]
f2 = least_square(x2, y2, 2)

pl.plot(x2, y2, label='f2 raw')
x2s = np.linspace(x2[0], x2[-1], 101)
pl.plot(x2s, list(f2(x) for x in x2s), label='f2 fitted')

pl.legend()
pl.show()

