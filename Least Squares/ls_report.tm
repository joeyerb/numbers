<TeXmacs|1.99.2>

<style|<tuple|generic|chinese>>

<\body>
  <doc-data|<doc-title|\<#8BA1\>\<#7B97\>\<#65B9\>\<#6CD5\>\<#53CA\>\<#5B9E\>\<#73B0\>\<#5B9E\>\<#9A8C\>\<#62A5\>\<#544A\>>|<doc-author|<author-data|<author-name|Joeyerb@bitbucket>>>>

  <section*|\<#5B9E\>\<#9A8C\>\<#4E94\>\<#3001\>\<#6700\>\<#5C0F\>\<#4E8C\>\<#4E58\>\<#6CD5\>>

  <subsection|\<#4EE3\>\<#7801\>>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    import numpy as np

    from scipy import linalg

    \;

    # \<#6700\>\<#5C0F\>\<#4E8C\>\<#4E58\>\<#6CD5\>\<#4E4B\>\<#4EE3\>\<#6570\>\<#591A\>\<#9879\>\<#5F0F\>\<#62DF\>\<#5408\>\ 

    # \<#53C2\>\<#6570\>n\<#FF1A\>\<#6700\>\<#9AD8\>\<#6B21\>\<#FF0C\>
    \<#8FD4\>\<#56DE\>\<#62DF\>\<#5408\>\<#51FD\>\<#6570\>

    def least_square(xs, ys, n):

    \;

    \ \ \ \ if len(xs) != len(ys):

    \ \ \ \ \ \ \ \ return

    \ \ \ \ m = len(xs)

    \;

    \ \ \ \ ms = []

    \ \ \ \ for j in range(2*n+1):

    \ \ \ \ \ \ \ \ s = 0

    \ \ \ \ \ \ \ \ for i in range(m):

    \ \ \ \ \ \ \ \ \ \ \ \ s += xs[i] ** j

    \ \ \ \ \ \ \ \ ms.append(s)

    \;

    \ \ \ \ bs = []

    \ \ \ \ for j in range(n+1):

    \ \ \ \ \ \ \ \ s = 0

    \ \ \ \ \ \ \ \ for i in range(m):

    \ \ \ \ \ \ \ \ \ \ \ \ s += xs[i] ** j * ys[i]

    \ \ \ \ \ \ \ \ bs.append(s)

    \;

    \ \ \ \ mtrx = []

    \ \ \ \ for j in range(n+1):

    \ \ \ \ \ \ \ \ row = []

    \ \ \ \ \ \ \ \ for i in range(n+1):

    \ \ \ \ \ \ \ \ \ \ \ \ row.append(ms[i+j])

    \ \ \ \ \ \ \ \ mtrx.append(row)

    \;

    \ \ \ \ sa = linalg.solve(np.array(mtrx), np.array(bs))

    \ \ \ \ print(sa)

    \;

    \ \ \ \ def f(x):

    \ \ \ \ \ \ \ \ s = 0

    \ \ \ \ \ \ \ \ for j in range(n+1):

    \ \ \ \ \ \ \ \ \ \ \ \ s += sa[j] * x**j

    \ \ \ \ \ \ \ \ return s

    \;

    \ \ \ \ return f
  </cpp-code>

  <page-break*>

  <subsection|\<#4E60\>\<#9898\>>

  <\exercise>
    (P106, \<#4F8B\>5) \<#4F7F\>\<#7535\>\<#6D41\> <math|i>
    \<#901A\>\<#8FC7\> <math|2 \<b-Omega\>>
    \<#7684\>\<#7535\>\<#963B\>\<#FF0C\> \<#7528\>\<#4F0F\>\<#7279\>\<#8868\>\<#6D4B\>\<#91CF\>\<#7535\>\<#963B\>\<#4E24\>\<#7AEF\>\<#7684\>\<#7535\>\<#538B\>
    <math|V>\<#FF0C\> \<#5F97\>\<#5230\>\<#5982\>\<#4E0B\>\<#6570\>\<#636E\>\<#FF1A\>

    <big-table|<tabular|<tformat|<cwith|1|-1|1|-1|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1px>|<cwith|1|-1|1|1|cell-rborder|1px>|<table|<row|<cell|
    \ <math|i> /A >|<cell|1>|<cell|2>|<cell|4>|<cell|6>|<cell|8>|<cell|10>>|<row|<cell|
    \ \ \ <math|V>/V \ \ \ >|<cell| \ \ 1.8 \ \ >|<cell| \ \ 3.7 \ \ >|<cell|
    \ \ 8.2 \ \ >|<cell| \ \ 12.0 \ \ >|<cell| \ \ 15.8 \ \ >|<cell| \ \ 20.2
    \ \ >>>>>|>

    \<#8BD5\>\<#7528\>\<#6700\>\<#5C0F\>\<#4E8C\>\<#4E58\>\<#6CD5\>\<#5EFA\>\<#7ACB\>
    <math|i> \<#4E0E\> <math|V> \<#4E4B\>\<#95F4\>\<#7684\>\<#7ECF\>\<#9A8C\>\<#516C\>\<#5F0F\>
    \<#FF0E\>

    <\solution>
      \;

      <\cpp-code>
        <\cpp-code>
          # \<#8F93\>\<#5165\>

          import pylab as pl

          \;

          x1 = [1.0, 2.0, 4.0, 6.0, 8.0, 10.0]

          y1 = [1.8, 3.7, 8.2, 12.0, 15.8, 20.2]

          f1 = least_square(x1, y1, 1)

          \;

          pl.plot(x1, y1, label='f1 raw')

          x1s = np.linspace(x1[0], x1[-1], 101)

          pl.plot(x1s, list(f1(x) for x in x1s), label='f1 fitted')

          \;

          pl.legend()

          pl.show()

          \;

          # \<#8F93\>\<#51FA\>

          [-0.21561644 2.03205479]
        </cpp-code>
      </cpp-code>

      <\itemize-arrow>
        <item>\<#6545\>\<#6240\>\<#6C42\>\<#7ECF\>\<#9A8C\>\<#516C\>\<#5F0F\>\<#4E3A\>

        <\equation*>
          V=-0.216+2.032i
        </equation*>
      </itemize-arrow>

      <image|figure_1.png|720px|540px||>
    </solution>
  </exercise>

  <page-break*>

  <\exercise>
    (P184, 10) \<#5DF2\>\<#77E5\>\<#4E00\>\<#7EC4\>\<#5B9E\>\<#9A8C\>\<#6570\>\<#636E\>

    <big-table|<tabular|<tformat|<cwith|1|-1|1|-1|cell-halign|c>|<cwith|2|2|1|-1|cell-bborder|1px>|<cwith|1|-1|1|1|cell-rborder|1px>|<cwith|1|1|1|-1|cell-bborder|1px>|<cwith|1|1|1|-1|cell-tborder|2px>|<cwith|3|3|1|-1|cell-bborder|2px>|<table|<row|<cell|<math|i>>|<cell|1>|<cell|2>|<cell|3>|<cell|4>|<cell|5>|<cell|6>|<cell|7>|<cell|8>|<cell|9>>|<row|<cell|<math|x<rsub|i>>>|<cell|1>|<cell|3>|<cell|4>|<cell|5>|<cell|6>|<cell|7>|<cell|8>|<cell|9>|<cell|10>>|<row|<cell|
    \ \ \ <math|y<rsub|i>> \ \ \ >|<cell| \ \ \ 10 \ \ \ >|<cell| \ \ \ 5
    \ \ \ >|<cell| \ \ \ 4 \ \ \ >|<cell| \ \ \ 2 \ \ \ >|<cell| \ \ \ 1
    \ \ \ >|<cell| \ \ \ 1 \ \ \ >|<cell| \ \ \ 2 \ \ \ >|<cell| \ \ \ 3
    \ \ \ >|<cell| \ \ \ 4 \ \ \ >>>>>|>

    \<#8BD5\>\<#7528\>\<#6700\>\<#5C0F\>\<#4E8C\>\<#4E58\>\<#6CD5\>\<#6C42\>\<#5B83\>\<#7684\>\<#591A\>\<#9879\>\<#5F0F\>\<#62DF\>\<#5408\>\<#66F2\>\<#7EBF\>\<#FF0C\>\<#5E76\>\<#6C42\>\<#51FA\>\<#6700\>\<#4F4E\>\<#70B9\>\<#7684\>\<#4F4D\>\<#7F6E\>.

    <\solution>
      \;

      \<#6839\>\<#636E\>\<#56FE\>\<#5F62\>\<#FF0C\>
      \<#9009\>\<#62E9\>\<#4E8C\>\<#6B21\>\<#51FD\>\<#6570\>\<#6765\>\<#62DF\>\<#5408\>

      <\cpp-code>
        # \<#8F93\>\<#5165\>

        x2 = [1, 3, 4, 5, 6, 7, 8, 9, 10]

        y2 = [10, 5, 4, 2, 1, 1, 2, 3, 4]

        f2 = least_square(x2, y2, 2)

        \;

        pl.plot(x2, y2, label='f2 raw')

        x2s = np.linspace(x2[0], x2[-1], 101)

        pl.plot(x2s, list(f2(x) for x in x2s), label='f2 fitted')

        pl.legend()

        pl.show()

        \;

        # \<#8F93\>\<#51FA\>

        [13.45966387 -3.6053094 0.26757066]

        # \<#6700\>\<#4F4E\>\<#70B9\>

        [6.73711634547 1.31496943278]
      </cpp-code>

      <\itemize-arrow>
        <item>\<#7531\>\<#6B64\>\<#FF0C\>
        \<#62DF\>\<#5408\>\<#51FD\>\<#6570\>\<#4E3A\>

        <\equation*>
          y=12.4597-3.6053x+0.2676x<rsup|2>
        </equation*>

        <item>\<#6700\>\<#4F4E\>\<#70B9\>\<#7EA6\>\<#4E3A\> (6.7371, 1.3150)
      </itemize-arrow>

      <image|figure_2.png|720px|540px||>
    </solution>
  </exercise>

  \;

  \;

  <\note>
    \<#672C\>\<#5B9E\>\<#9A8C\>\<#4F7F\>\<#7528\>Python\<#8BED\>\<#8A00\>
  </note>

  <\note>
    \<#6E90\>\<#4EE3\>\<#7801\>\<#5730\>\<#5740\>\<#FF1A\>https://bitbucket.org/joeyerb/numbers/
  </note>
</body>

<\references>
  <\collection>
    <associate|TB_ajaxContent|<tuple|?|?>>
    <associate|TB_ajaxWindowTitle|<tuple|?|?>>
    <associate|TB_closeAjaxWindow|<tuple|?|?>>
    <associate|TB_closeWindowButton|<tuple|?|?>>
    <associate|TB_title|<tuple|?|?>>
    <associate|TB_window|<tuple|?|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|3.2|3>>
    <associate|auto-11|<tuple|1.3|4>>
    <associate|auto-12|<tuple|1.3.1|?>>
    <associate|auto-13|<tuple|1.3.2|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|2>>
    <associate|auto-4|<tuple|1|2>>
    <associate|auto-5|<tuple|2|3>>
    <associate|auto-6|<tuple|3.2|2>>
    <associate|auto-7|<tuple|3.2|2>>
    <associate|auto-8|<tuple|3|3>>
    <associate|auto-9|<tuple|3.1|3>>
    <associate|container|<tuple|?|?>>
    <associate|debuginfo|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|2>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|2>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|left|<tuple|?|?>>
    <associate|right|<tuple|?|?>>
    <associate|table1|<tuple|?|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|table>
      <tuple|normal||<pageref|auto-4>>

      <tuple|normal||<pageref|auto-5>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#5B9E\>\<#9A8C\>\<#4E94\>\<#3001\>\<#6700\>\<#5C0F\>\<#4E8C\>\<#4E58\>\<#6CD5\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>\<#4EE3\>\<#7801\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|2<space|2spc>\<#4E60\>\<#9898\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>