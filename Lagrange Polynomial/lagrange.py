#!/usr/bin/env python3

def lagrange(X, Y, n, x):
    N = len(X)
    if n < 2 or n > N:
        return
    if n < N:
        ds = []
        rg = []
        for i in range(N):
            ds.append((i, abs(x-X[i])))
        for d in sorted(ds, key=lambda p: p[1]):
            rg.append(d[0])
        rg = sorted(rg[:n])
    else:
        rg = range(N)
    print('x =', list(map(lambda p: X[p], rg)))

    y = 0
    for k in rg:
        P = 1.0
        for j in rg:
            if j != k:
                P = P * (x - X[j]) / (X[k] - X[j])
        y += P * Y[k]
    print(x, y, '\n')

# P78.L1
X = [.1, .2, .3, .4, .5]
Y = [1.1052, 1.2214, 1.3499, 1.4918, 1.6487]

lagrange(X, Y, 2, .285)
lagrange(X, Y, 3, .285)
lagrange(X, Y, 5, .285)

# P183.8
X = [.0, .1, .195, .3, .401, .5]
Y = [.39894, .39695, .39142, .38138, .36812, .35206]

lagrange(X, Y, 2, .15)
lagrange(X, Y, 3, .15)
lagrange(X, Y, 6, .15)

lagrange(X, Y, 2, .31)
lagrange(X, Y, 3, .31)
lagrange(X, Y, 6, .31)

lagrange(X, Y, 2, .47)
lagrange(X, Y, 3, .47)
lagrange(X, Y, 6, .47)

