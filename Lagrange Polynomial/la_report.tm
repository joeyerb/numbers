<TeXmacs|1.99.2>

<style|<tuple|generic|chinese>>

<\body>
  <doc-data|<doc-title|\<#8BA1\>\<#7B97\>\<#65B9\>\<#6CD5\>\<#53CA\>\<#5B9E\>\<#73B0\>\<#5B9E\>\<#9A8C\>\<#62A5\>\<#544A\>>|<doc-author|<author-data|<author-name|Joeyerb@bitbucket>>>>

  <section*|\<#5B9E\>\<#9A8C\>\<#56DB\>\<#3001\>\<#62C9\>\<#683C\>\<#6717\>\<#65E5\>\<#63D2\>\<#503C\>\<#591A\>\<#9879\>\<#5F0F\>>

  <subsection|\<#4EE3\>\<#7801\>>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    # \<#5DF2\>\<#77E5\> X\<#3001\> Y\<#FF0C\> \<#6C42\>\<#5173\>\<#4E8E\> x
    \<#7684\> n \<#6B21\>\<#63D2\>\<#503C\>\<#FF0C\>
    \<#8FD4\>\<#56DE\>\<#503C\> L(x)

    def lagrange(X, Y, n, x):

    \ \ \ \ N = len(X)

    \ \ \ \ if n \<less\> 2 or n \<gtr\> N:

    \ \ \ \ \ \ \ \ return

    \ \ \ \ if n \<less\> N:

    \ \ \ \ \ \ \ \ ds = []

    \ \ \ \ \ \ \ \ rg = []

    \ \ \ \ \ \ \ \ for i in range(N):

    \ \ \ \ \ \ \ \ \ \ \ \ ds.append((i, abs(x-X[i])))

    \ \ \ \ \ \ \ \ for d in sorted(ds, key=lambda p: p[1]):

    \ \ \ \ \ \ \ \ \ \ \ \ rg.append(d[0])\ 

    \ \ \ \ \ \ \ \ rg = sorted(rg[:n])

    \ \ \ \ else:

    \ \ \ \ \ \ \ \ rg = range(N)

    \ \ \ \ print('x =', list(map(lambda p: X[p], rg)))

    \;

    \ \ \ \ y = 0

    \ \ \ \ for k in rg:

    \ \ \ \ \ \ \ \ P = 1.0

    \ \ \ \ \ \ \ \ for j in rg:

    \ \ \ \ \ \ \ \ \ \ \ \ if j != k:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ P = P * (x - X[j]) / (X[k] - X[j])

    \ \ \ \ \ \ \ \ y += P * Y[k]

    \ \ \ \ print(x, y, '\\n')

    \;

    \;
  </cpp-code>

  <subsection|\<#4E60\>\<#9898\>>

  <\exercise>
    (P78, \<#4F8B\>1) \<#7ED9\>\<#5B9A\>\<#51FD\>\<#6570\>\<#8868\>\<#5982\>\<#4E0B\>\<#FF1A\>

    <big-table|<tabular|<tformat|<cwith|1|-1|1|-1|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1px>|<cwith|1|-1|1|1|cell-rborder|1px>|<table|<row|<cell|<math|x>>|<cell|...>|<cell|0.1>|<cell|0.2>|<cell|0.3>|<cell|0.4>|<cell|0.5>|<cell|...>>|<row|<cell|
    \ \ \ <math|e<rsup|x>> \ \ \ >|<cell| \ \ ... \ \ >|<cell| 1.1052
    >|<cell| 1.2214 >|<cell| 1.3499 >|<cell| 1.3918 >|<cell| 1.6387 >|<cell|
    \ \ ... \ \ >>>>>|>

    \<#8BD5\>\<#7528\>\<#7EBF\>\<#6027\>\<#63D2\>\<#503C\>\<#4E0E\>\<#629B\>\<#7269\>\<#63D2\>\<#503C\>\<#6C42\>
    <math|e<rsup|0.285>> \<#7684\>\<#8FD1\>\<#4F3C\>\<#503C\>\<#FF0C\>\<#5E76\>\<#4F30\>\<#8BA1\>\<#622A\>\<#65AD\>\<#8BEF\>\<#5DEE\>\<#FF0E\>

    <\solution>
      \;

      <\cpp-code>
        <\cpp-code>
          # \<#8F93\>\<#5165\>

          X = [.1, .2, .3, .4, .5]

          Y = [1.1052, 1.2214, 1.3499, 1.4918, 1.6487]

          \;

          lagrange(X, Y, 2, .285)

          lagrange(X, Y, 3, .285)

          lagrange(X, Y, 5, .285)

          \;

          # \<#8F93\>\<#51FA\>

          x = [0.2, 0.3]

          0.285 1.330625\ 

          \;

          x = [0.2, 0.3, 0.4]

          0.285 1.32977075\ 

          \;

          x = [0.1, 0.2, 0.3, 0.4, 0.5]

          0.285 1.329803282421875
        </cpp-code>
      </cpp-code>

      \;
    </solution>
  </exercise>

  \;

  <\exercise>
    (P183, 8) \<#4ECE\>\<#51FD\>\<#6570\>\<#8868\>

    <big-table|<tabular|<tformat|<cwith|1|-1|1|-1|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1px>|<cwith|1|-1|1|1|cell-rborder|1px>|<table|<row|<cell|<math|x>>|<cell|0.0>|<cell|0.1>|<cell|0.195>|<cell|0.3>|<cell|0.401>|<cell|0.5>>|<row|<cell|
    \ \ \ <math|f<around*|(|x|)>> \ \ \ >|<cell| 0.39894 >|<cell| 0.39695
    >|<cell| 0.39142 >|<cell| 0.38138 >|<cell| 0.36812 >|<cell| 0.35206
    >>>>>|>

    \<#51FA\>\<#53D1\>\<#FF0C\>\<#7528\>\<#4E0B\>\<#5217\>\<#65B9\>\<#6CD5\>\<#8BA1\>\<#7B97\>
    <math|f<around*|(|0.15|)>>\<#FF0C\><math|f<around*|(|0.31|)>> \<#53CA\>
    <math|f<around*|(|0.47|)>> \<#7684\>\<#8FD1\>\<#4F3C\>\<#503C\>\<#FF1A\>

    <\enumerate-numeric>
      <item>\<#5206\>\<#6BB5\>\<#7EBF\>\<#6027\>\<#63D2\>\<#503C\>\<#FF1B\>

      <item>\<#5206\>\<#6BB5\>\<#4E8C\>\<#6B21\>\<#63D2\>\<#503C\>\<#FF1B\>

      <item>\<#5168\>\<#533A\>\<#95F4\>\<#4E0A\>\<#62C9\>\<#683C\>\<#6717\>\<#65E5\>\<#63D2\>\<#503C\>
    </enumerate-numeric>

    <\solution>
      \;

      <\cpp-code>
        # \<#8F93\>\<#5165\>

        X = [.0, .1, .195, .3, .401, .5]

        Y = [.39894, .39695, .39142, .38138, .36812, .35206]

        \;

        lagrange(X, Y, 2, .15)

        lagrange(X, Y, 3, .15)

        lagrange(X, Y, 6, .15)

        \;

        lagrange(X, Y, 2, .31)

        lagrange(X, Y, 3, .31)

        lagrange(X, Y, 6, .31)

        \;

        lagrange(X, Y, 2, .47)

        lagrange(X, Y, 3, .47)

        lagrange(X, Y, 6, .47)

        \;

        \;

        # \<#8F93\>\<#51FA\>

        # f(0.15):

        x = [0.1, 0.195]

        0.15 0.39403947368421055\ 

        \;

        x = [0.0, 0.1, 0.195]

        0.15 0.39448151821862343\ 

        \;

        x = [0.0, 0.1, 0.195, 0.3, 0.401, 0.5]

        0.15 0.3944728038780606\ 

        \;

        # f(0.31):

        x = [0.3, 0.401]

        0.31 0.38006712871287124\ 

        \;

        x = [0.195, 0.3, 0.401]

        0.31 0.38022469159537314\ 

        \;

        x = [0.0, 0.1, 0.195, 0.3, 0.401, 0.5]

        0.31 0.3802190624547322\ 

        \;

        # f(0.47):

        x = [0.401, 0.5]

        0.47 0.35692666666666667\ 

        \;

        x = [0.3, 0.401, 0.5]

        0.47 0.35724684488448843\ 

        \;

        x = [0.0, 0.1, 0.195, 0.3, 0.401, 0.5]

        0.47 0.3572221123394848

        \;
      </cpp-code>

      \;
    </solution>
  </exercise>

  <\note>
    \<#672C\>\<#5B9E\>\<#9A8C\>\<#4F7F\>\<#7528\>Python\<#8BED\>\<#8A00\>
  </note>

  <\note>
    \<#6E90\>\<#4EE3\>\<#7801\>\<#5730\>\<#5740\>\<#FF1A\>https://bitbucket.org/joeyerb/numbers/
  </note>
</body>

<\references>
  <\collection>
    <associate|TB_ajaxContent|<tuple|?|?>>
    <associate|TB_ajaxWindowTitle|<tuple|?|?>>
    <associate|TB_closeAjaxWindow|<tuple|?|?>>
    <associate|TB_closeWindowButton|<tuple|?|?>>
    <associate|TB_title|<tuple|?|?>>
    <associate|TB_window|<tuple|?|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|3.2|3>>
    <associate|auto-11|<tuple|1.3|4>>
    <associate|auto-12|<tuple|1.3.1|?>>
    <associate|auto-13|<tuple|1.3.2|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|1>>
    <associate|auto-4|<tuple|1|1>>
    <associate|auto-5|<tuple|2|2>>
    <associate|auto-6|<tuple|3.2|2>>
    <associate|auto-7|<tuple|3.2|2>>
    <associate|auto-8|<tuple|3|3>>
    <associate|auto-9|<tuple|3.1|3>>
    <associate|container|<tuple|?|?>>
    <associate|debuginfo|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|2>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|2>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|left|<tuple|?|?>>
    <associate|right|<tuple|?|?>>
    <associate|table1|<tuple|?|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#5B9E\>\<#9A8C\>\<#4E09\>\<#3001\>\<#96C5\>\<#53EF\>\<#6BD4\>\<#8FED\>\<#4EE3\>\<#6CD5\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>\<#4EE3\>\<#7801\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|2<space|2spc>\<#4E60\>\<#9898\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>