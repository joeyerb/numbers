<TeXmacs|1.99.2>

<style|<tuple|generic|chinese>>

<\body>
  <doc-data|<doc-title|\<#8BA1\>\<#7B97\>\<#65B9\>\<#6CD5\>\<#53CA\>\<#5B9E\>\<#73B0\>\<#5B9E\>\<#9A8C\>\<#62A5\>\<#544A\>>|<doc-author|<author-data|<author-name|Joeyerb@bitbucket>>>>

  <section*|\<#5B9E\>\<#9A8C\>\<#516D\>\<#3001\>\<#725B\>\<#987F\>--\<#79D1\>\<#8328\>\<#516C\>\<#5F0F\>>

  <subsection|\<#590D\>\<#5408\>\<#68AF\>\<#5F62\>\<#516C\>\<#5F0F\>>

  <subsubsection|\<#4EE3\>\<#7801\>>

  <\cpp-code>
    #!/usr/bin/python3

    \;

    def trapezoid_kai(f, a, b, e, k0, k1):

    \ \ \ \ T = {}

    \ \ \ \ T[0] = (b - a) / 2 * (f(a) + f(b))

    \ \ \ \ for k in range(1, k0 + 1):

    \ \ \ \ \ \ \ \ T[k] = T[k-1] / 2\ 

    \ \ \ \ \ \ \ \ for i in range(1, 2**(k-1) + 1):

    \ \ \ \ \ \ \ \ \ \ \ \ T[k] += (b - a) / 2**k * f(a + (2*i - 1) * (b -
    a) / 2**k)

    \ \ \ \ \ \ \ \ print(k, 2**k, T[k])

    \ \ \ \ \ \ \ \ if k \<gtr\>= k1 and abs(T[k] - T[k-1]) \<less\> 3 * e:

    \ \ \ \ \ \ \ \ \ \ \ \ return T[k]

    \ \ \ \ print('Failed')

    \ \ \ \ return 0

    \;

    \;
  </cpp-code>

  <subsubsection|\<#4E60\>\<#9898\>>

  <\exercise>
    (P184, 13) \<#7528\>\<#590D\>\<#5408\>\<#68AF\>\<#5F62\>\<#6CD5\>\<#9012\>\<#63A8\>\<#7B97\>\<#5F0F\>\<#8BA1\>\<#7B97\>\<#79EF\>\<#5206\>:
    <math|<big|int><rsup|1><rsub|0><dfrac|*<around*|(|1-e<rsup|-x>|)><rsup|<frac|1|2>>|x>\<b-up-d\>x>\<#FF0C\>\<#4F7F\>\<#8BEF\>\<#5DEE\>\<#4E0D\>\<#8D85\>\<#8FC7\><math|10<rsup|-4>>

    <\solution>
      \<#539F\>\<#5F0F\>\<#4E3A\>\<#53CD\>\<#5E38\>\<#79EF\>\<#5206\>\<#FF0C\>\<#4EE4\><math|x=t<rsup|2>>\<#FF0C\>\<#5219\>\<#6709\>

      <\equation*>
        <big|int><rsup|1><rsub|0><dfrac|*<around*|(|1-e<rsup|-x>|)><rsup|<frac|1|2>>|x>\<b-up-d\>x
        = <big|int><rsup|1><rsub|0><dfrac|*2<around*|(|1-e<rsup|-t<rsup|2>>|)><rsup|<frac|1|2>>|t>\<b-up-d\>t
      </equation*>

      <\description>
        <item*|\<#5B9A\>\<#4E49\>\<#51FD\>\<#6570\>><cpp|f = lambda x:
        2*(1-e**-x**2)**.5/x if x != 0 else 2>

        <item*|\<#8F93\>\<#5165\>\<#8BA1\>\<#7B97\>><cpp|trapezoid_kai(f, 0,
        1, .00001, 20, 4)>

        <item*|\<#8F93\>\<#51FA\>\<#7ED3\>\<#679C\>>

        <\cpp-code>
          1 2 1.8381664651340714

          2 4 1.848644461402974

          3 8 1.8512466403912995

          4 16 1.8518961215057068

          5 32 1.852058425594508

          6 64 1.8520989974843063

          7 128 1.852109140198545

          \;

          1.852109140198545
        </cpp-code>
      </description>

      <\itemize-arrow>
        <item>\<#5F97\>\<#5230\>\<#7ED3\>\<#679C\>\<#4E3A\>1.8521
      </itemize-arrow>

      \;
    </solution>
  </exercise>

  \;

  <subsection|\<#590D\>\<#5408\>\<#8F9B\>\<#666E\>\<#68EE\>\<#516C\>\<#5F0F\>>

  <subsubsection|\<#4EE3\>\<#7801\>>

  <\cpp-code>
    #!/usr/bin/python3

    \;

    def simpson(f, a, b, n):

    \ \ \ \ h = (b - a) / (2 * n)

    \ \ \ \ x = a

    \ \ \ \ S = f(x) - f(b)

    \ \ \ \ for k in range(1, n+1):

    \ \ \ \ \ \ \ \ x += h

    \ \ \ \ \ \ \ \ S += 4 * f(x)

    \ \ \ \ \ \ \ \ x += h

    \ \ \ \ \ \ \ \ S += 2 * f(x)

    \ \ \ \ return S * h / 3

    \;

    def simpson_kai(f, a, b, e, k0, k1):

    \ \ \ \ S = {}

    \ \ \ \ S[0] = simpson(f, a, b, 1)

    \ \ \ \ for k in range(1, k0 + 1):

    \ \ \ \ \ \ \ \ S[k] = simpson(f, a, b, 2**k)

    \ \ \ \ \ \ \ \ print(k, 2**k, S[k])

    \ \ \ \ \ \ \ \ if k \<gtr\>= k1 and abs(S[k] - S[k-1]) \<less\> 15 * e:

    \ \ \ \ \ \ \ \ \ \ \ \ return S[k]

    \ \ \ \ print('Failed')

    \ \ \ \ return 0

    \;

    \;
  </cpp-code>

  <subsubsection|\<#4E60\>\<#9898\>>

  <\exercise>
    (P184, 14) \<#7528\>\<#590D\>\<#5408\>\<#8F9B\>\<#666E\>\<#68EE\>\<#516C\>\<#5F0F\>\<#8BA1\>\<#7B97\>\<#79EF\>\<#5206\>
    <math|<big|int><rsup|48><rsub|0><sqrt|1+cos<rsup|2>x>\<b-up-d\>x>\<#FF0C\>\<#4F7F\>\<#8BEF\>\<#5DEE\>\<#4E0D\>\<#8D85\>\<#8FC7\><math|10<rsup|-4>>
  </exercise>

  <\solution>
    \;

    <\description>
      <item*|\<#5B9A\>\<#4E49\>\<#51FD\>\<#6570\>><cpp|g = lambda x:
      sqrt(1+cos(x)**2)>

      <item*|\<#8F93\>\<#5165\>\<#8BA1\>\<#7B97\>><cpp|simpson_kai(g, 0, 48,
      0.00001, 20, 4)>

      <item*|\<#8F93\>\<#51FA\>\<#7ED3\>\<#679C\>>

      <\cpp-code>
        1 2 56.16214702973425

        2 4 56.20282264402147

        3 8 56.20401200301779

        4 16 59.07732072079879

        5 32 58.4796956264747

        6 64 58.470611495375955

        7 128 58.47048067242013

        \;

        58.47048067242013
      </cpp-code>
    </description>

    <\itemize-arrow>
      <item>\<#5F97\>\<#5230\>\<#7ED3\>\<#679C\>\<#4E3A\>58.4705
    </itemize-arrow>
  </solution>

  \;

  \;

  <\note>
    \<#672C\>\<#5B9E\>\<#9A8C\>\<#4F7F\>\<#7528\>Python\<#8BED\>\<#8A00\>
  </note>

  <\note>
    \<#6E90\>\<#4EE3\>\<#7801\>\<#5730\>\<#5740\>\<#FF1A\>https://bitbucket.org/joeyerb/numbers
  </note>
</body>

<\references>
  <\collection>
    <associate|TB_ajaxContent|<tuple|?|?>>
    <associate|TB_ajaxWindowTitle|<tuple|?|?>>
    <associate|TB_closeAjaxWindow|<tuple|?|?>>
    <associate|TB_closeWindowButton|<tuple|?|?>>
    <associate|TB_title|<tuple|?|?>>
    <associate|TB_window|<tuple|?|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|3.2|3>>
    <associate|auto-11|<tuple|1.3|4>>
    <associate|auto-12|<tuple|1.3.1|?>>
    <associate|auto-13|<tuple|1.3.2|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|1.1|1>>
    <associate|auto-4|<tuple|1.2|1>>
    <associate|auto-5|<tuple|2|2>>
    <associate|auto-6|<tuple|2.1|2>>
    <associate|auto-7|<tuple|2.2|2>>
    <associate|auto-8|<tuple|3|3>>
    <associate|auto-9|<tuple|3.1|3>>
    <associate|container|<tuple|?|?>>
    <associate|debuginfo|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|2>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|2>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|left|<tuple|?|?>>
    <associate|right|<tuple|?|?>>
    <associate|table1|<tuple|?|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#5B9E\>\<#9A8C\>\<#516D\>\<#3001\>\<#725B\>\<#987F\>--\<#79D1\>\<#8328\>\<#516C\>\<#5F0F\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>\<#590D\>\<#5408\>\<#68AF\>\<#5F62\>\<#516C\>\<#5F0F\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|2tab>|1.1<space|2spc>\<#4EE3\>\<#7801\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|2tab>|1.2<space|2spc>\<#4E60\>\<#9898\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|2<space|2spc>\<#590D\>\<#5408\>\<#8F9B\>\<#666E\>\<#68EE\>\<#516C\>\<#5F0F\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|2tab>|2.1<space|2spc>\<#4EE3\>\<#7801\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|2tab>|2.2<space|2spc>\<#4E60\>\<#9898\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>
    </associate>
  </collection>
</auxiliary>