#!/usr/bin/python3

def simpson(f, a, b, n):
    h = (b - a) / (2 * n)
    x = a
    S = f(x) - f(b)
    for k in range(1, n+1):
        x += h
        S += 4 * f(x)
        x += h
        S += 2 * f(x)
    return S * h / 3

def trapezoid(f, a, b, n):
    h = (b - a) / n
    x = a
    T = f(a) + f(b)
    for k in range(1, n):
        x += h
        T += 2 * f(x)
    return T * h / 2


def trapezoid_kai(f, a, b, e, k0, k1):
    T = {}
    T[0] = (b - a) / 2 * (f(a) + f(b))
    for k in range(1, k0 + 1):
        T[k] = T[k-1] / 2 
        for i in range(1, 2**(k-1) + 1):
            T[k] += (b - a) / 2**k * f(a + (2*i - 1) * (b - a) / 2**k)
        print(k, 2**k, T[k])
        if k >= k1 and abs(T[k] - T[k-1]) < 3 * e:
            return T[k]
    print('Failed')
    return 0

def simpson_kai(f, a, b, e, k0, k1):
    S = {}
    S[0] = simpson(f, a, b, 1)
    for k in range(1, k0 + 1):
        S[k] = simpson(f, a, b, 2**k)
        print(k, 2**k, S[k])
        if k >= k1 and abs(S[k] - S[k-1]) < 15 * e:
            return S[k]
    print('Failed')
    return 0


# Tests

from math import e, pi, sin, cos, sqrt

f = lambda x: 2*(1-e**-x**2)**.5/x if x != 0 else 2
print(trapezoid_kai(f, 0, 1, .00001, 20, 4), '\n')

g = lambda x: sqrt(1+cos(x)**2)
print(simpson_kai(g, 0, 48, 0.0001, 20, 4))
