#!/usr/bin/env python3

def gepp(n, A, b, e):
    for k in range(0, n-1):
        # Pivoting
        ik = k
        for i in range(k+1, n):
            if A[i][k] > A[ik][k]:
                ik = i

        if abs(A[ik][k]) < e:
            return 'det A = 0'

        if ik != k:
            for j in range(0, n):
                (A[k][j], A[ik][j]) = (A[ik][j], A[k][j])
            (b[k], b[ik]) = (b[ik], b[k])

        # Elimination
        for i in range(k+1, n):
            A[i][k] = 1.0 * A[i][k] / A[k][k]
            for j in range(k+1, n):
                A[i][j] = A[i][j] - 1.0 * A[i][k] * A[k][j]
            b[i] = b[i] - 1.0 * A[i][k] * b[k]

    if abs(A[n-1][n-1]) < e:
        return 'det A = 0'

    # Calculation
    b[n-1] = b[n-1] * 1.0 / A[n-1][n-1]
    for ii in range(1, n):
        i = n - ii - 1
        s = 0
        for j in range(i+1, n):
            s = s + A[i][j] * b[j] * 1.0
        b[i] = (b[i] - s) * 1.0 / A[i][i]
    return b

A1 = [
        [-.002, 2, 2],
        [1, .78125, 0],
        [3.996, 5.5625, 4]
     ]
b1 = [.4, 1.3816, 7.4178]

A2 = [
        [1, 1, 1],
        [1, 3, -2],
        [2, -2, 1]
     ]
b2 = [6, 1, 1]

print(gepp(3, A1, b1, 0.00001))
print(gepp(3, A2, b2, 0.00001))
