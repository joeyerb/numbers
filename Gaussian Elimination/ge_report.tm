<TeXmacs|1.99.2>

<style|<tuple|generic|chinese>>

<\body>
  <doc-data|<doc-title|\<#8BA1\>\<#7B97\>\<#65B9\>\<#6CD5\>\<#53CA\>\<#5B9E\>\<#73B0\>\<#5B9E\>\<#9A8C\>\<#62A5\>\<#544A\>>|<doc-author|<author-data|<author-name|Joeyerb@bitbucket>>>>

  <section*|\<#5B9E\>\<#9A8C\>\<#4E8C\>\<#3001\>\<#5217\>\<#4E3B\>\<#5143\>\<#9AD8\>\<#65AF\>\<#6D88\>\<#53BB\>\<#6CD5\>>

  <subsection|\<#8D34\>\<#4EE3\>\<#7801\>>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    def gepp(n, A, b, e):

    \ \ \ \ for k in range(0, n-1):

    \ \ \ \ \ \ \ \ # Pivoting

    \ \ \ \ \ \ \ \ ik = k

    \ \ \ \ \ \ \ \ for i in range(k+1, n):

    \ \ \ \ \ \ \ \ \ \ \ \ if A[i][k] \<gtr\> A[ik][k]:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ik = i

    \;

    \ \ \ \ \ \ \ \ if abs(A[ik][k]) \<less\> e:

    \ \ \ \ \ \ \ \ \ \ \ \ return 'det A = 0'

    \;

    \ \ \ \ \ \ \ \ if ik != k:

    \ \ \ \ \ \ \ \ \ \ \ \ for j in range(0, n):

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (A[k][j], A[ik][j]) = (A[ik][j], A[k][j])

    \ \ \ \ \ \ \ \ \ \ \ \ (b[k], b[ik]) = (b[ik], b[k])

    \;

    \ \ \ \ \ \ \ \ # Elimination

    \ \ \ \ \ \ \ \ for i in range(k+1, n):

    \ \ \ \ \ \ \ \ \ \ \ \ A[i][k] = 1.0 * A[i][k] / A[k][k]

    \ \ \ \ \ \ \ \ \ \ \ \ for j in range(k+1, n):

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ A[i][j] = A[i][j] - 1.0 * A[i][k] *
    A[k][j]

    \ \ \ \ \ \ \ \ \ \ \ \ b[i] = b[i] - 1.0 * A[i][k] * b[k]

    \;

    \ \ \ \ if abs(A[n-1][n-1]) \<less\> e:

    \ \ \ \ \ \ \ \ return 'det A = 0'

    \;

    \ \ \ \ # Calculation

    \ \ \ \ b[n-1] = b[n-1] * 1.0 / A[n-1][n-1]

    \ \ \ \ for ii in range(1, n):

    \ \ \ \ \ \ \ \ i = n - ii - 1

    \ \ \ \ \ \ \ \ s = 0

    \ \ \ \ \ \ \ \ for j in range(i+1, n):

    \ \ \ \ \ \ \ \ \ \ \ \ s = s + A[i][j] * b[j] * 1.0

    \ \ \ \ \ \ \ \ b[i] = (b[i] - s) * 1.0 / A[i][i]

    \ \ \ \ return b

    \;

    # P40, \<#4F8B\>2

    A1 = [

    \ \ \ \ \ \ \ \ [-.002, 2, 2],

    \ \ \ \ \ \ \ \ [1, .78125, 0],

    \ \ \ \ \ \ \ \ [3.996, 5.5625, 4]

    \ \ \ \ \ ]

    b1 = [.4, 1.3816, 7.4178]

    \;

    print(gepp(3, A1, b1, 0.00001))

    \;

    # P36\<#FF0C\> \<#4F8B\>1

    A2 = [

    \ \ \ \ \ \ \ \ [1, 1, 1],

    \ \ \ \ \ \ \ \ [1, 3, -2],

    \ \ \ \ \ \ \ \ [2, -2, 1]

    \ \ \ \ \ ]

    b2 = [6, 1, 1]

    \;

    print(gepp(3, A2, b2, 0.00001))

    \;
  </cpp-code>

  <subsection|\<#770B\>\<#7ED3\>\<#679C\>>

  <\cpp-code>
    [1.9273000000000002, -0.6984960000000006, 0.9004233000000005]

    [1.0, 2.0, 3.0]

    \;
  </cpp-code>

  \<#7B26\>\<#5408\>\<#4E8B\>\<#5B9E\>\<#3002\>

  <\note>
    \<#672C\>\<#5B9E\>\<#9A8C\>\<#4F7F\>\<#7528\>Python\<#8BED\>\<#8A00\>\<#FF0C\>\<#9664\>\<#4E86\>\<#6570\>\<#7EC4\>\<#7D22\>\<#5F15\>\<#4ECE\>0\<#5F00\>\<#59CB\>\<#5916\>\<#FF0C\>\<#6309\>\<#7167\>\<#4E66\>\<#672C\>\<#6846\>\<#56FE\>\<#7F16\>\<#5199\>\<#3002\>
  </note>

  <\note>
    \<#6E90\>\<#4EE3\>\<#7801\>\<#5730\>\<#5740\>\<#FF1A\>https://bitbucket.org/joeyerb/numbers
  </note>
</body>

<\references>
  <\collection>
    <associate|TB_ajaxContent|<tuple|?|?>>
    <associate|TB_ajaxWindowTitle|<tuple|?|?>>
    <associate|TB_closeAjaxWindow|<tuple|?|?>>
    <associate|TB_closeWindowButton|<tuple|?|?>>
    <associate|TB_title|<tuple|?|?>>
    <associate|TB_window|<tuple|?|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|3.2|3>>
    <associate|auto-11|<tuple|1.3|4>>
    <associate|auto-12|<tuple|1.3.1|?>>
    <associate|auto-13|<tuple|1.3.2|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|2>>
    <associate|auto-4|<tuple|1|2>>
    <associate|auto-5|<tuple|1.1|2>>
    <associate|auto-6|<tuple|2|2>>
    <associate|auto-7|<tuple|2.1|3>>
    <associate|auto-8|<tuple|3|3>>
    <associate|auto-9|<tuple|3.1|3>>
    <associate|container|<tuple|?|?>>
    <associate|debuginfo|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|2>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|2>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|left|<tuple|?|?>>
    <associate|right|<tuple|?|?>>
    <associate|table1|<tuple|?|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#5B9E\>\<#9A8C\>\<#4E8C\>\<#3001\>\<#5217\>\<#4E3B\>\<#5143\>\<#9AD8\>\<#65AF\>\<#6D88\>\<#53BB\>\<#6CD5\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>\<#8D34\>\<#4EE3\>\<#7801\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|2<space|2spc>\<#770B\>\<#7ED3\>\<#679C\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>