#!/usr/bin/env python3

def jacobi(n, A, b, E, N0, x):
    for k in range(1, N0+1):
        y = {}
        for i in range(0, n):
            sum_ax = 0
            for j in range(0, n):
                if i != j:
                    sum_ax += A[i][j] * x[j]
            y[i] = 1.0 / A[i][i] * (b[i] - sum_ax)
        e = max (abs(x[i] - y[i]) for i in range(0, n))
        for i in range(0, n):
            x[i] = y[i]
        if abs(e) < E:
            print(k, x)
            return x
        if k >= N0:
            print('Failed')
            return 0

A = [[3.0, 1.0], [1.0, 2.0]]
b = [5.0, 5.0]
x0 = [0, 0]

jacobi(2, A, b, 0.001, 20, x0)


A = [
        [4.0, -1.0, 0.0, -1.0, 0.0, 0.0],
        [-1.0, 4.0, -1.0, 0.0, -1.0, 0.0],
        [0.0, -1.0, 4.0, -1.0, 0.0, -1.0],
        [-1.0, 0.0, -1.0, 4.0, -1.0, 0.0],
        [0.0, -1.0, 0.0, -1.0, 4.0, -1.0],
        [0.0, 0.0, -1.0, 0.0, -1.0, 4.0]
    ]
b = [0.0, 5.0, -2.0, 5.0, -2.0, 6.0]
x0 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

jacobi(6, A, b, 0.0001, 40, x0)
