<TeXmacs|1.99.2>

<style|<tuple|generic|chinese>>

<\body>
  <doc-data|<doc-title|\<#8BA1\>\<#7B97\>\<#65B9\>\<#6CD5\>\<#53CA\>\<#5B9E\>\<#73B0\>\<#5B9E\>\<#9A8C\>\<#62A5\>\<#544A\>>|<doc-author|<author-data|<author-name|Joeyerb@bitbucket>>>>

  <section*|\<#5B9E\>\<#9A8C\>\<#4E09\>\<#3001\>\<#96C5\>\<#53EF\>\<#6BD4\>\<#8FED\>\<#4EE3\>\<#6CD5\>>

  <subsection|\<#4EE3\>\<#7801\>>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    # \<#53C2\>\<#6570\>\<#FF1A\> A\<#FF1A\>
    n\<#9636\>\<#77E9\>\<#9635\>\<#FF0C\> b\<#FF1A\>
    n\<#5143\>\<#5411\>\<#91CF\>\<#FF0C\> E\<#FF1A\>
    \<#7CBE\>\<#5EA6\>\<#63A7\>\<#5236\>\<#FF0C\> N0\<#FF1A\>
    \<#6700\>\<#5927\>\<#8FED\>\<#4EE3\>\<#6B21\>\<#6570\>\<#FF0C\>
    x\<#FF1A\> \<#521D\>\<#503C\>

    def jacobi(n, A, b, E, N0, x):

    \ \ \ \ for k in range(1, N0+1):

    \ \ \ \ \ \ \ \ y = {}

    \ \ \ \ \ \ \ \ for i in range(0, n):

    \ \ \ \ \ \ \ \ \ \ \ \ sum_ax = 0

    \ \ \ \ \ \ \ \ \ \ \ \ for j in range(0, n):

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ if i != j:

    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ sum_ax += A[i][j] * x[j]

    \ \ \ \ \ \ \ \ \ \ \ \ y[i] = 1.0 / A[i][i] * (b[i] - sum_ax)

    \ \ \ \ \ \ \ \ e = max (abs(x[i] - y[i]) for i in range(0, n))

    \ \ \ \ \ \ \ \ for i in range(0, n):

    \ \ \ \ \ \ \ \ \ \ \ \ x[i] = y[i]

    \ \ \ \ \ \ \ \ if abs(e) \<less\> E:

    \ \ \ \ \ \ \ \ \ \ \ \ print(k, x)

    \ \ \ \ \ \ \ \ \ \ \ \ return x

    \ \ \ \ \ \ \ \ if k \<gtr\>= N0:

    \ \ \ \ \ \ \ \ \ \ \ \ print('Failed')

    \ \ \ \ \ \ \ \ \ \ \ \ return -1

    \;
  </cpp-code>

  <subsection|\<#4E60\>\<#9898\>>

  <\exercise>
    (P55, \<#4F8B\>8) \<#7528\>\<#8FED\>\<#4EE3\>\<#6CD5\>\<#89E3\>\<#7EBF\>\<#6027\>\<#65B9\>\<#7A0B\>\<#7EC4\>

    <\eqnarray*>
      <tformat|<table|<row|<cell|<choice|<tformat|<table|<row|<cell|3x<rsub|1>+x<rsub|2>=5>>|<row|<cell|x<rsub|1>+2x<rsub|2>=5>>>>>>|<cell|>|<cell|<eq-number>>>>>
    </eqnarray*>

    <\solution>
      \;

      <\cpp-code>
        <\cpp-code>
          # \<#8F93\>\<#5165\>

          A = [[3.0, 1.0], [1.0, 2.0]]

          b = [5.0, 5.0]

          x0 = [0.0, 0.0]

          \;

          jacobi(2, A, b, 0.001, 20, x0)

          \;

          # \<#8F93\>\<#51FA\>\<#7ED3\>\<#679C\>

          10 [0.9998713991769548, 1.9997427983539096]

          \;
        </cpp-code>
      </cpp-code>

      <\itemize-arrow>
        <item>\<#7531\>\<#6B64\>\<#5224\>\<#65AD\>\<#FF0C\>\<#89E3\>\<#4E3A\>
        X=[1, 2]
      </itemize-arrow>

      \;
    </solution>
  </exercise>

  \;

  <\exercise>
    (P182, 7) \<#5206\>\<#6790\>\<#7528\>\<#4E0B\>\<#5217\>\<#8FED\>\<#4EE3\>\<#6CD5\>\<#89E3\>\<#7EBF\>\<#6027\>\<#65B9\>\<#7A0B\>\<#7EC4\>

    <\eqnarray*>
      <tformat|<table|<row|<cell|<matrix|<tformat|<cwith|1|1|1|1|cell-hyphen|n>|<cwith|1|-1|1|-1|cell-halign|r>|<table|<row|<cell|4>|<cell|-1>|<cell|0>|<cell|-1>|<cell|0>|<cell|0>>|<row|<cell|-1>|<cell|4>|<cell|-1>|<cell|0>|<cell|-1>|<cell|0>>|<row|<cell|0>|<cell|-1>|<cell|4>|<cell|-1>|<cell|0>|<cell|-1>>|<row|<cell|-1>|<cell|0>|<cell|-1>|<cell|4>|<cell|-1>|<cell|0>>|<row|<cell|0>|<cell|-1>|<cell|0>|<cell|-1>|<cell|4>|<cell|-1>>|<row|<cell|0>|<cell|0>|<cell|-1>|<cell|0>|<cell|-1>|<cell|4>>>>><matrix|<tformat|<table|<row|<cell|x<rsub|1>>>|<row|<cell|x<rsub|2>>>|<row|<cell|x<rsub|3>>>|<row|<cell|x<rsub|4>>>|<row|<cell|x<rsub|5>>>|<row|<cell|x<rsub|6>>>>>>>|<cell|=>|<cell|<matrix|<tformat|<cwith|6|6|1|1|cell-halign|r>|<cwith|1|4|1|1|cell-halign|r>|<table|<row|<cell|0>>|<row|<cell|5>>|<row|<cell|-2>>|<row|<cell|5>>|<row|<cell|-2>>|<row|<cell|6>>>>><eq-number>>>>>
    </eqnarray*>

    \<#7684\>\<#6536\>\<#655B\>\<#6027\>\<#FF0C\>\<#5E76\>\<#6C42\>\<#51FA\>\<#4F7F\>
    <math|<around*|\<\|\|\>|\<b-X\><rsup|<around*|(|k+1|)>>-\<b-X\><rsup|<around*|(|k|)>>|\<\|\|\>><rsub|2>
    \<leqslant\> 0.0001> \<#7684\>\<#8FD1\>\<#4F3C\>\<#89E3\>\<#53CA\>\<#76F8\>\<#5E94\>\<#7684\>\<#8FED\>\<#4EE3\>\<#6B21\>\<#6570\>.
  </exercise>

  <\solution>
    \;

    <\cpp-code>
      # \<#8F93\>\<#5165\>

      A = [

      \ \ \ \ \ \ \ \ [4.0, -1.0, 0.0, -1.0, 0.0, 0.0],

      \ \ \ \ \ \ \ \ [-1.0, 4.0, -1.0, 0.0, -1.0, 0.0],

      \ \ \ \ \ \ \ \ [0.0, -1.0, 4.0, -1.0, 0.0, -1.0],

      \ \ \ \ \ \ \ \ [-1.0, 0.0, -1.0, 4.0, -1.0, 0.0],

      \ \ \ \ \ \ \ \ [0.0, -1.0, 0.0, -1.0, 4.0, -1.0],

      \ \ \ \ \ \ \ \ [0.0, 0.0, -1.0, 0.0, -1.0, 4.0]

      \ \ \ \ ]

      b = [0.0, 5.0, -2.0, 5.0, -2.0, 6.0]

      x0 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

      \;

      jacobi(6, A, b, 0.0001, 40, x0)

      \;

      # \<#8F93\>\<#51FA\>\<#7ED3\>\<#679C\>

      27 [0.9999466044318979, 1.9999635301487615, 0.999927060297523,
      1.9999635301487615, 0.999927060297523, 1.999973302215949]

      \;
    </cpp-code>

    <\itemize-arrow>
      <item>\<#8FED\>\<#4EE3\>\<#6B21\>\<#6570\>\<#4E3A\>27\<#FF0C\>\<#6839\>\<#636E\>\<#8FD1\>\<#4F3C\>\<#89E3\>\<#5F97\>
      X=[1, 2, 1, 2, 1, 2]<math|<rsup|T>>.
    </itemize-arrow>
  </solution>

  \;

  \;

  <\note>
    \<#672C\>\<#5B9E\>\<#9A8C\>\<#4F7F\>\<#7528\>Python\<#8BED\>\<#8A00\>
  </note>

  <\note>
    \<#6E90\>\<#4EE3\>\<#7801\>\<#5730\>\<#5740\>\<#FF1A\>https://bitbucket.org/joeyerb/numbers
  </note>
</body>

<\references>
  <\collection>
    <associate|TB_ajaxContent|<tuple|?|?>>
    <associate|TB_ajaxWindowTitle|<tuple|?|?>>
    <associate|TB_closeAjaxWindow|<tuple|?|?>>
    <associate|TB_closeWindowButton|<tuple|?|?>>
    <associate|TB_title|<tuple|?|?>>
    <associate|TB_window|<tuple|?|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|3.2|3>>
    <associate|auto-11|<tuple|1.3|4>>
    <associate|auto-12|<tuple|1.3.1|?>>
    <associate|auto-13|<tuple|1.3.2|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|1>>
    <associate|auto-4|<tuple|2.1|1>>
    <associate|auto-5|<tuple|3.1|2>>
    <associate|auto-6|<tuple|3.2|2>>
    <associate|auto-7|<tuple|3.2|2>>
    <associate|auto-8|<tuple|3|3>>
    <associate|auto-9|<tuple|3.1|3>>
    <associate|container|<tuple|?|?>>
    <associate|debuginfo|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|2>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|2>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|left|<tuple|?|?>>
    <associate|right|<tuple|?|?>>
    <associate|table1|<tuple|?|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#5B9E\>\<#9A8C\>\<#4E09\>\<#3001\>\<#96C5\>\<#53EF\>\<#6BD4\>\<#8FED\>\<#4EE3\>\<#6CD5\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>\<#4EE3\>\<#7801\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|2<space|2spc>\<#4E60\>\<#9898\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>