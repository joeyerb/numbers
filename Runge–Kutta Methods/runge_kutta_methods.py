#!/usr/bin/env python3

import numpy as np

def euler_method(a, b, y0, N, f):
    h = np.float64((b - a) / N)
    x = a
    y = y0
    print(x, y)
    for k in range(N):
        y += h * f(x, y)
        x += h
        print(x, y)

def euler_method_kai(a, b, y0, N, f):
    h = np.float64((b - a) / N)
    x = a
    y = y0
    print(x, y)
    for k in range(N):
        T1 = y + h * f(x, y)
        x += h
        T2 = y + h * f(x, T1)
        y = (T1 + T2) / 2
        print(x, y)

def runge_kutta(a, b, y0, N, f):
    h = np.float64((b - a) / N)
    x = a
    y = y0
    print(x, y)
    for k in range(N):
        K1 = f(x, y)
        x += h/2
        K2 = f(x, y+h/2*K1)
        K3 = f(x, y+h/2*K2)
        x += h/2
        K4 = f(x, y+h*K3)
        y += h * (K1 + 2*K2 + 2*K3 + K4) / 6
        print(x, y)

# Test (P149 L1)
f = lambda x, y : x - y + 1
euler_method(0, 0.5, 1, 5, f)
euler_method_kai(0, 0.5, 1, 5, f)
runge_kutta(0, 0.5, 1, 5, f)

# Test2 (P157 L4)
g = lambda x, y: -y
euler_method(0, 1.0, 1, 40, g)
euler_method_kai(0, 1.0, 1, 20, g)
runge_kutta(0, 1.0, 1, 10, g)

# Test3 (P185 17)
f = lambda x, y: 2 / 3 * x * y ** -2
euler_method(0, 1.0, 1, 10, f)
euler_method_kai(0, 1.0, 1, 10, f)
runge_kutta(0, 1.0, 1, 10, f)
