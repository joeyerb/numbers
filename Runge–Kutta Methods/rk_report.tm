<TeXmacs|1.99.2>

<style|<tuple|generic|chinese>>

<\body>
  <doc-data|<doc-title|\<#8BA1\>\<#7B97\>\<#65B9\>\<#6CD5\>\<#53CA\>\<#5B9E\>\<#73B0\>\<#5B9E\>\<#9A8C\>\<#62A5\>\<#544A\>>|<doc-author|<author-data|<author-name|Joeyerb@bitbucket>>>>

  <section*|\<#5B9E\>\<#9A8C\>\<#4E03\>\<#3001\>\<#9F99\>\<#683C\>\<longminus\>\<#5E93\>\<#5854\>\<#6CD5\>>

  <subsection|\<#4EE3\>\<#7801\>>

  <\cpp-code>
    #!/usr/bin/env python3

    \;

    import numpy as np

    \;

    # \<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>\<#FF0C\>
    \<#53C2\>\<#6570\>\<#FF1A\> \<#8303\>\<#56F4\>[a, b],
    \<#521D\>\<#503C\>y0, \<#7B49\>\<#5206\>\<#6570\>N\<#FF0C\>
    \<#51FD\>\<#6570\>f(x, y)

    def euler_method(a, b, y0, N, f):

    \ \ \ \ h = np.float64((b - a) / N) \ \ \ #//
    h\<#4F7F\>\<#7528\>64\<#4F4D\>\<#6D6E\>\<#70B9\>\<#6570\>\<#4EE5\>\<#786E\>\<#4FDD\>\<#7CBE\>\<#5EA6\>

    \ \ \ \ x = a

    \ \ \ \ y = y0

    \ \ \ \ print(x, y)

    \ \ \ \ for k in range(N):

    \ \ \ \ \ \ \ \ y += h * f(x, y)

    \ \ \ \ \ \ \ \ x += h

    \ \ \ \ \ \ \ \ print(x, y)

    \;

    # \<#6539\>\<#8FDB\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>,
    \<#53C2\>\<#6570\>\<#540C\>\<#4E0A\>

    def euler_method_kai(a, b, y0, N, f):

    \ \ \ \ h = np.float64((b - a) / N)

    \ \ \ \ x = a

    \ \ \ \ y = y0

    \ \ \ \ print(x, y)

    \ \ \ \ for k in range(N):

    \ \ \ \ \ \ \ \ T1 = y + h * f(x, y)

    \ \ \ \ \ \ \ \ x += h

    \ \ \ \ \ \ \ \ T2 = y + h * f(x, T1)

    \ \ \ \ \ \ \ \ y = (T1 + T2) / 2

    \ \ \ \ \ \ \ \ print(x, y)

    \;

    # \<#9F99\>\<#683C\>\<longminus\>\<#5E93\>\<#5854\>\<#6CD5\>\<#FF0C\>
    \<#53C2\>\<#6570\>\<#540C\>\<#4E0A\>

    def runge_kutta(a, b, y0, N, f):

    \ \ \ \ h = np.float64((b - a) / N)

    \ \ \ \ x = a

    \ \ \ \ y = y0

    \ \ \ \ print(x, y)

    \ \ \ \ for k in range(N):

    \ \ \ \ \ \ \ \ K1 = f(x, y)

    \ \ \ \ \ \ \ \ x += h/2

    \ \ \ \ \ \ \ \ K2 = f(x, y+h/2*K1)

    \ \ \ \ \ \ \ \ K3 = f(x, y+h/2*K2)

    \ \ \ \ \ \ \ \ x += h/2

    \ \ \ \ \ \ \ \ K4 = f(x, y+h*K3)

    \ \ \ \ \ \ \ \ y += h * (K1 + 2*K2 + 2*K3 + K4) / 6

    \ \ \ \ \ \ \ \ print(x, y)

    \;
  </cpp-code>

  \;

  <subsection|\<#4E60\>\<#9898\>>

  <\exercise>
    (P149, \<#4F8B\>1) \<#7528\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>\<#6C42\>\<#521D\>\<#503C\>\<#95EE\>\<#9898\>

    <\equation*>
      <choice|<tformat|<cwith|1|1|1|1|cell-halign|c>|<table|<row|<cell|y<rprime|'>
      >|<cell|=>|<cell| x-y+1,>|<cell|x\<in\><around*|[|0,0.5|]>>>|<row|<cell|y<around*|(|0|)>>|<cell|=>|<cell|1>|<cell|>>>>>
    </equation*>

    \<#7684\>\<#6570\>\<#503C\>\<#89E3\>\<#FF08\>\<#53D6\>h=0.1\<#FF09\>\<#FF0E\>

    <\solution>
      \;

      <\cpp-code>
        <\cpp-code>
          # \<#8F93\>\<#5165\>

          f = lambda x, y : x - y + 1

          euler_method(0, 0.5, 1, 5, f)

          euler_method_kai(0, 0.5, 1, 5, f)

          runge_kutta(0, 0.5, 1, 5, f)

          \;

          # \<#8F93\>\<#51FA\>

          # 1. \<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>

          0 1

          0.1 1.0

          0.2 1.01

          0.3 1.029

          0.4 1.0561

          0.5 1.09049

          \;

          # 2. \<#6539\>\<#8FDB\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>

          0 1

          0.1 1.005

          0.2 1.019025

          0.3 1.041217625

          0.4 1.07080195063

          0.5 1.10707576532

          \;

          # 3. \<#9F99\>\<#683C\>\<longminus\>\<#5E93\>\<#5854\>\<#6CD5\>

          0 1

          0.1 1.0048375

          0.2 1.01873090141

          0.3 1.040818422

          0.4 1.07032028892

          0.5 1.10653093442
        </cpp-code>
      </cpp-code>

      \;
    </solution>
  </exercise>

  \;

  <\exercise>
    (P157, \<#4F8B\>4) \<#5206\>\<#522B\>\<#7528\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>\<#FF08\>h=0.025\<#FF09\>\<#3001\>\<#6539\>\<#8FDB\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>\<#FF08\>h=0.05\<#FF09\>\<#53CA\>\<#7ECF\>\<#5178\>R-K\<#6CD5\>\<#FF08\>h=0.1\<#FF09\>\<#6C42\>\<#89E3\>\<#521D\>\<#503C\>\<#95EE\>\<#9898\>

    <\equation*>
      <choice|<tformat|<cwith|1|1|1|1|cell-halign|c>|<cwith|2|2|1|1|cell-height|32px>|<cwith|2|2|1|1|cell-vmode|exact>|<table|<row|<cell|<dfrac|dy|dx>>|<cell|=>|<cell|-y,>|<cell|x\<in\><around*|[|0,1|]>>>|<row|<cell|y<around*|(|0|)>>|<cell|=>|<cell|1>|<cell|>>>>>
    </equation*>

    .

    <\solution>
      \;

      <\cpp-code>
        # \<#8F93\>\<#5165\>

        g = lambda x, y: -y

        euler_method(0, 1.0, 1, 40, g)

        euler_method_kai(0, 1.0, 1, 20, g)

        runge_kutta(0, 1.0, 1, 10, g)

        \;

        # \<#8F93\>\<#51FA\>

        # 1. \<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>

        0 1

        0.1 0.903687890625

        0.2 0.816651803662

        0.3 0.737998345827

        0.4 0.666920168425

        0.5 0.602687680219

        0.6 0.544641558443

        0.7 0.492185981096

        0.8 0.444782511052

        0.9 0.401944569199

        1.0 0.363232439888

        \;

        # 2. \<#6539\>\<#8FDB\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>

        0 1

        0.1 0.9048765625

        0.2 0.818801593362

        0.3 0.740914371171

        0.4 0.670436049292

        0.5 0.606661867659

        0.6 0.548954105407

        0.7 0.496735703871

        0.8 0.44948449619

        0.9 0.40672798581

        1.0 0.368038621672

        \;

        # 3. \<#9F99\>\<#683C\>\<longminus\>\<#5E93\>\<#5854\>\<#6CD5\>

        0 1

        0.1 0.9048375

        0.2 0.818730901406

        0.3 0.740818422001

        0.4 0.670320288917

        0.5 0.606530934423

        0.6 0.548811934376

        0.7 0.496585618671

        0.8 0.449329289734

        0.9 0.4065699912

        1.0 0.367879774412
      </cpp-code>

      \;
    </solution>
  </exercise>

  <\exercise>
    (P185, 17) \<#5206\>\<#522B\>\<#7528\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>\<#3001\>
    \<#6539\>\<#8FDB\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>\<#53CA\>\<#7ECF\>\<#5178\>R-K\<#6CD5\>\<#FF08\>h=0.1\<#FF09\>,
    \<#6C42\>\<#89E3\>\<#521D\>\<#503C\>\<#95EE\>\<#9898\>

    <\equation*>
      <choice|<tformat|<cwith|1|1|1|1|cell-halign|c>|<cwith|2|2|1|1|cell-valign|b>|<cwith|2|2|1|1|cell-height|32px>|<cwith|2|2|1|1|cell-vmode|exact>|<table|<row|<cell|<dfrac|dy|dx>>|<cell|=>|<cell|<dfrac|2|3>
      x y<rsup|-2>,>|<cell|x\<in\><around*|[|0,1|]>>>|<row|<cell|y<around*|(|0|)>>|<cell|=>|<cell|1>|<cell|>>>>>
    </equation*>

    .

    <\solution>
      \;

      <\cpp-code>
        # \<#8F93\>\<#5165\>

        f = lambda x, y: 2 / 3 * x * y ** -2

        euler_method(0, 1.0, 1, 10, f)

        euler_method_kai(0, 1.0, 1, 10, f)

        runge_kutta(0, 1.0, 1, 10, f)

        \;

        # \<#8F93\>\<#51FA\>

        <tabular|<tformat|<table|<row|<cell|x
        \ \ \ \ >|<cell|\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>
        \ \ \ \ \ \ \ \ \ >|<cell|\<#6539\>\<#8FDB\>\<#6B27\>\<#62C9\>\<#65B9\>\<#6CD5\>
        \ \ \ \ \ >|<cell|\<#9F99\>\<#683C\>\<longminus\>\<#5E93\>\<#5854\>\<#6CD5\>
        \ \ \ \ >>|<row|<cell|0>|<cell|1>|<cell|1>|<cell|1>>|<row|<cell|0.1>|<cell|1.0>|<cell|1.00333333333>|<cell|1.00332229272>>|<row|<cell|0.2>|<cell|1.00666666667>|<cell|1.0131804344>|<cell|1.0131594382>>|<row|<cell|0.3>|<cell|1.01982398433>|<cell|1.02917124455>|<cell|1.02914253544>>|<row|<cell|0.4>|<cell|1.03905399621>|<cell|1.05075108>|<cell|1.05071767902>>|<row|<cell|0.5>|<cell|1.06375374284>|<cell|1.07725231061>|<cell|1.07721748>>|<row|<cell|0.6>|<cell|1.09321128738>|<cell|1.10796505336>|<cell|1.10793180837>>|<row|<cell|0.7>|<cell|1.12668098409>|<cell|1.14219413569>|<cell|1.14216492938>>|<row|<cell|0.8>|<cell|1.1634434684>|<cell|1.17929728422>|<cell|1.17927388378>>|<row|<cell|0.9>|<cell|1.20284455113>|<cell|1.21870557556>|<cell|1.21868908341>>|<row|<cell|1.0>|<cell|1.2443143797>|<cell|1.25993026586>|<cell|1.25992122158>>>>>

        \;

        \;
      </cpp-code>

      \;
    </solution>
  </exercise>

  \;

  <\note>
    \<#672C\>\<#5B9E\>\<#9A8C\>\<#4F7F\>\<#7528\>Python\<#8BED\>\<#8A00\>
  </note>

  <\note>
    \<#6E90\>\<#4EE3\>\<#7801\>\<#5730\>\<#5740\>\<#FF1A\>https://bitbucket.org/joeyerb/numbers/
  </note>
</body>

<\initial>
  <\collection>
    <associate|font-base-size|12>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|TB_ajaxContent|<tuple|?|?>>
    <associate|TB_ajaxWindowTitle|<tuple|?|?>>
    <associate|TB_closeAjaxWindow|<tuple|?|?>>
    <associate|TB_closeWindowButton|<tuple|?|?>>
    <associate|TB_title|<tuple|?|?>>
    <associate|TB_window|<tuple|?|?>>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|3.2|3>>
    <associate|auto-11|<tuple|1.3|4>>
    <associate|auto-12|<tuple|1.3.1|?>>
    <associate|auto-13|<tuple|1.3.2|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|2>>
    <associate|auto-4|<tuple|1|2>>
    <associate|auto-5|<tuple|2|3>>
    <associate|auto-6|<tuple|3.2|2>>
    <associate|auto-7|<tuple|3.2|2>>
    <associate|auto-8|<tuple|3|3>>
    <associate|auto-9|<tuple|3.1|3>>
    <associate|container|<tuple|?|?>>
    <associate|debuginfo|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|2>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|2>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|left|<tuple|?|?>>
    <associate|right|<tuple|?|?>>
    <associate|table1|<tuple|?|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|\<#5B9E\>\<#9A8C\>\<#516D\>\<#3001\>\<#9F99\>\<#683C\>\<longminus\>\<#5E93\>\<#5854\>\<#6CD5\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1<space|2spc>\<#4EE3\>\<#7801\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|2<space|2spc>\<#4E60\>\<#9898\>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>